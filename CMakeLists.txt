cmake_minimum_required(VERSION 3.7)

if(TARGET serialconsole)
    return()
endif()

project(serialconsole)

include(ExternalProject)

set(CMAKE_C_STANDARD 99)
set(CMAKE_SYSTEM_NAME Linux)

set(DEBUG ON CACHE BOOL "Enable DEBUG build")
set(STRIP OFF CACHE BOOL "Strip binary")
set(CROSS_COMPILE OFF CACHE BOOL "Build for flight")

function(set_compile_options target)
    target_compile_options(${target} PRIVATE
        -std=c99
        -Werror
        -Wfatal-errors
        -pedantic-errors
        -Wextra
        -Wall
        -Winit-self
        -Wmissing-include-dirs
        -Wswitch-default
        -Wswitch
        -Wfloat-equal
        -Wshadow
        -Wunsafe-loop-optimizations
        -Wcast-qual
        -Wconversion
        -Wlogical-op
        -Waggregate-return
        -Wstrict-prototypes
        -Wmissing-prototypes
        -Wmissing-declarations
        -Wnormalized=nfc
        -Wpacked
        -Wpadded
        -Wnested-externs
        -Wunreachable-code
        -Wno-format
        -Wno-error=unused-function
        -Wno-error=sign-conversion
        -Wno-error=padded
        -Wno-error=unsafe-loop-optimizations
        -fno-strict-aliasing
    )
endfunction()

function(set_debug_options target debug cross)
    if(${debug})
        target_compile_options(${target} PRIVATE
            -Og
            -g
            -DDEBUG
        )
    else()
        target_compile_options(${target} PRIVATE
            -O3
            -Winline
            -DNDEBUG
        )
    endif()
endfunction()

set(FS_SRC_SERIALCONSOLE_NAME serialconsole)
set(FS_SRC_SERIALCONSOLE_FILE serialconsole)

set(FS_SRC_SERIALCONSOLE_SRC ${PROJECT_SOURCE_DIR}/src/serialconsole.c)
set(FS_SRC_SERIALCONSOLE_HEADER ${PROJECT_SOURCE_DIR}/src/serialconsole.h)

add_executable(${FS_SRC_SERIALCONSOLE_NAME} ${FS_SRC_SERIALCONSOLE_SRC} ${FS_SRC_SERIALCONSOLE_HEADER})

# this is an old tool, don't need to meet all warnings
# set_compile_options(${FS_SRC_SERIALCONSOLE_NAME})

set_debug_options(${FS_SRC_SERIALCONSOLE_NAME} ${DEBUG} ${CROSS_COMPILE})

target_include_directories(${FS_SRC_SERIALCONSOLE_NAME} PUBLIC ${PROJECT_SOURCE_DIR}/src)

add_subdirectory(${PROJECT_SOURCE_DIR}/libdebug)
add_subdirectory(${PROJECT_SOURCE_DIR}/libtty)
add_subdirectory(${PROJECT_SOURCE_DIR}/libhmt)
add_subdirectory(${PROJECT_SOURCE_DIR}/libfiso)
add_subdirectory(${PROJECT_SOURCE_DIR}/libcslog)

add_dependencies(${FS_SRC_SERIALCONSOLE_NAME} debug_lib)
add_dependencies(${FS_SRC_SERIALCONSOLE_NAME} tty)
add_dependencies(${FS_SRC_SERIALCONSOLE_NAME} hmt)
add_dependencies(${FS_SRC_SERIALCONSOLE_NAME} fiso)
add_dependencies(${FS_SRC_SERIALCONSOLE_NAME} cslog)

target_link_libraries(${FS_SRC_SERIALCONSOLE_NAME} PRIVATE debug_lib)
target_link_libraries(${FS_SRC_SERIALCONSOLE_NAME} PRIVATE tty)
target_link_libraries(${FS_SRC_SERIALCONSOLE_NAME} PRIVATE hmt)
target_link_libraries(${FS_SRC_SERIALCONSOLE_NAME} PRIVATE fiso)
target_link_libraries(${FS_SRC_SERIALCONSOLE_NAME} PRIVATE cslog)

if(${CROSS_COMPILE})
    set(CMAKE_SYSTEM_PROCESSOR arm)
    set(CROSS_COMPILER_PREFIX "/usr")
    
    set(CROSS_COMPILER_BINDIR ${CROSS_COMPILER_PREFIX}/bin)
    set(CROSS_COMPILER_INCLUDE ${CROSS_COMPILER_PREFIX}/arm-linux-gnueabihf/include)
    
    set(CMAKE_AR ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ar CACHE FILEPATH "Archiver")
    set(CMAKE_CXX_COMPILER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-g++)
    set(CMAKE_C_COMPILER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-gcc)
    set(CMAKE_LINKER ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ld)
    set(CMAKE_NM ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-nm)
    set(CMAKE_OBJCOPY ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-objcopy)
    set(CMAKE_OBJDUMP ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-objdump)
    set(CMAKE_RANLIB ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-ranlib CACHE FILEPATH "Runlib")
    set(CMAKE_STRIP ${CROSS_COMPILER_BINDIR}/arm-linux-gnueabihf-strip)

    set(CMAKE_FIND_ROOT_PATH ${CROSS_COMPILER_PREFIX}/arm-linux-gnueabihf)

    set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

    if(${STRIP})
        add_custom_command(TARGET ${FS_SRC_SERIALCONSOLE_NAME}
            POST_BUILD
            COMMAND ${CMAKE_STRIP} --strip-all ${FS_SRC_SERIALCONSOLE_FILE}
        )
    endif()
else()
    if(${STRIP})
        add_custom_command(TARGET ${FS_SRC_SERIALCONSOLE_NAME}
            POST_BUILD
            COMMAND strip --strip-all ${FS_SRC_SERIALCONSOLE_FILE}
        )
    endif()
endif()
