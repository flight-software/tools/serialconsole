#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <libgen.h>
#include <sys/time.h>

#include "libdebug.h"                                       
#include "libtty.h" 
#include "libhmt.h"
#include "libfiso.h"
#include "libcslog.h"
#include "serialconsole.h"

/**
 * Buffers and logic variables
 */

#define BIG_BUF_LEN 65536
uint8_t input_buffer[BIG_BUF_LEN];
uint8_t output_buffer[BIG_BUF_LEN];
uint8_t command_buffer[256];

/**
 * Structures
 */
typedef struct {
    char name[100];
    char command[2*sizeof command_buffer];
    char expected_resp[2*sizeof output_buffer];
    char actual_resp[2*sizeof output_buffer];
    char comment[100];
    int status;
    char err_string[BIG_BUF_LEN];
} serial_command_test;

int count, sfd; /* serial file descriptor */

// Global variable configuration
char device_name[256];
char program_name[SYSWIDE_PATH_LEN_MAX];
char command_sched_filename[SYSWIDE_PATH_LEN_MAX];
int vmin = SERIAL_VMIN;
int vtime = SERIAL_VTIME;
speed_t baud_rate = SERIAL_BAUD();
int enable_rtscts = 0;
int enable_rs485 = 0;
int wait_time = SERIAL_WAIT_TIME; /* microseconds to wait before
                        * sending in automatic mode */
int read_ascii = 0;
int console_mode = INTERACTIVE_CONSOLE;
int quiet_mode = 0;
#define EXPECTED_RESP_IMPL
int check_response = 0;
int serial_timeout = SERIAL_SELECT_TIMEOUT;

/**
 * Bookkeeping variables
 */
serial_command_test current_test;
ssize_t total_tests = -1;
ssize_t passed_tests = -1;

FILE * schedule_log = NULL;

void usage()
{
    /**
     * ./serialConsole [device]
     *      [-f schedule_file]
     *      [-b baud_rate]
     *      [-k custom_baud_rate]
     *      [-m vmin]
     *      [-T vtime]
     *      [-r (RTC/CTS)]
     *      [-C (check response)]
     *      [-h (help)]
     *      [-w wait_time (in microseconds)]
     *      [-4 (RS485)]
     *      [-q (quiet mode, only print errors)]
     *      [-t serial_timeout (ms)]
     */
    fprintf(stdout, "%s usage:\n\n", program_name);
    fprintf(stdout, "Run this program to execute arbitrary serial\n");
    fprintf(stdout, "command grammars, simply by specifying the hexadecimal\n");
    fprintf(stdout, "message you want to send to the device. The response \n");
    fprintf(stdout, "will then be printed in hexadecimal.\n");
    fprintf(stdout, "\n%s [/path/to/serial/device] [OPTIONS]\n",
                    program_name);
    fprintf(stdout, "\n    OPTIONS\n");
    fprintf(stdout, "\n\t[-h]\n"
                    "\t\tPrint this help message.\n");
    fprintf(stdout, "\n\t[-f /path/to/schedule/file]\n"
                    "\t\tRun the console in automatic mode, sending\n"
                    "\t\tevery command in the schedule file in order, waiting\n"
                    "\t\twait_time microseconds in between transmissions\n");
    fprintf(stdout, "\n\t[-b baud_rate]\n"
                    "\t\tSet the baud rate to any of the standard rates \n"
                    "\t\tbetween 9600 and 115200\n"
                    "\t\tDefaults to %s\n", SERIAL_BAUD_STR);
    fprintf(stdout, "\n\t[-k custom_baud_rate]\n"
                    "\t\tSet the baud rate directly to any specified rate\n");
    fprintf(stdout, "\n\t[-m vmin]\n"
                    "\t\tSet the minimum number of bytes requires to return\n"
                    "\t\tfrom a serial response.\n"
                    "\t\tDefaults to %d\n", SERIAL_VMIN);
    fprintf(stdout, "\n\t[-T vtime]\n"
                    "\t\tSet the number of tenths of a second we wait before\n"
                    "\t\tconsidering a session terminating.\n"
                    "\t\tDefaults to %d, which corresponds to %.1f seconds\n"
                    , SERIAL_VTIME, (double)SERIAL_VTIME*0.1);
    fprintf(stdout, "\n\t[-r]\n"
                    "\t\tEnable RTS/CTS for this instrument.\n"
                    "\t\tDefaults to off.\n");
    fprintf(stdout, "\n\t[-4]\n"
                    "\t\tEnable RS485 for the serial connection\n");
    fprintf(stdout, "\n\t[-w wait_time]\n"
                    "\t\tPeriod of time, in microseconds, to wait between\n"
                    "\t\tsending automated serial commands\n"
                    "\t\tDefaults to %d\n"
                    , SERIAL_WAIT_TIME);
    fprintf(stdout, "\n\t[-q]\n"
                    "\t\tEnable quiet mode, in which success is never\n"
                    "\t\treported. One time messages and errors are printed.\n"
                    );
    fprintf(stdout, "\n\t[-t serial_timeout]\n"
                    "\t\tSet the period of time after a write before a read\n"
                    "\t\tis considered timed out. Uses units of milliseconds.\n"
                    "\t\tDefaults to %d\n"
                    , SERIAL_SELECT_TIMEOUT
                    );
    fprintf(stdout, "\n\t[-c]\n"
                    "\t\tForce checking of expected response according to\n"
                    "\t\tschedule file. See NOTES\n"
                    );
    fprintf(stdout, "\n    NOTES\n");
    fprintf(stdout, "\n\tThe format of the command schedule file is a CSV, "
                    "with\n"
                    "\tthe following entries:\n"
#ifdef EXPECTED_RESP_IMPL
                    "\n\tcommand,expected response\n"
#else
                    "\n\tcommand\n"
#endif
                    "\n\tThe \"command\" field is a hexadecimal string "
                    "representing\n"
                    "\tthe buffer you wish to send to the serial port, with\n"
                    "\tthe leftmost pair of hex characters occupying the \n"
                    "\tleast significant address of the buffer, and the\n"
                    "\trightmost pair occupying the most significant address.\n"
                    "\t  As an example, the string \"ff00aabc\" would map to\n"
                    "\ta buffer that looks like:\n"
                    "\n\t\tbuf[0] = 0xff;\n"
                    "\t\tbuf[1] = 0x00;\n"
                    "\t\tbuf[2] = 0xaa;\n"
                    "\t\tbuf[3] = 0xbc;\n"
#ifdef EXPECTED_RESP_IMPL
                    "\n\t  The \"expected response\" string exists to allow\n"
                    "\tyou to verify that the response matches what your\n"
                    "\tcommand grammar dictates it should be.\n"
                    "\tYou may specify it as \"*\", which means any response\n"
                    "\tis acceptable, or you may specify a hexadecimal string\n"
                    "\tlike you did for the command.\n"
#endif
                    );
    fprintf(stdout, "\n\tIf you want to scroll through this help message,\n"
                    "\tsimply pipe it into `less`, like so:\n"
                    "\n\t\t%s -h | less\n\n"
                    , program_name);

}

int scanInputIntoCommand(char * pt)
{
    if(read_ascii) {
        memcpy(command_buffer, input_buffer, ARRAY_SIZE(command_buffer));
    } else {
        if(removeChar(pt, ' ')) {
            printf("Unable to remove spaces\n");
            return EXIT_FAILURE;
        } else {
            QPRINT("Removed spaces: %s\n", pt);
        }
        if(fillHexBufFromString(command_buffer, pt, BIG_BUF_LEN)) {
            printf("Unable to fill buffer from string\n");
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}

int tokenizeScheduleLine(char * line, const char delimiters[])
{
    char *tofree, *token, *subrow;

    tofree = subrow = strdup(line);

#define SCAN_TOKEN(dst)         \
    token = strsep(&subrow, delimiters);                       \
    if(token == NULL) token = "NULL";                          \
    strlcpy((dst), token, sizeof((dst)))

    SCAN_TOKEN(current_test.name);
    SCAN_TOKEN(current_test.command);
    SCAN_TOKEN(current_test.expected_resp);
    SCAN_TOKEN(current_test.comment);

    free(tofree);
    return EXIT_SUCCESS;
}

int checkExpectedResponse()
{
    const char * token = current_test.expected_resp;
    const size_t token_len = strlen(token);

    // token has the expected response as hex
    // no real bounds checking on if the string is valid hex; beware of this!

    // TODO: use wordexpr to make this better
    if(token[0] == '*' && token[1] == '\0'){
        //QPRINT("All responses accepted for this command!\n");
        return EXIT_SUCCESS;
    }

    if(token_len == 0 || (token_len % 2)) {
        printf("Invalid expected response (wrong length): %s\n", token);
        APPEND_ERR("Invalid length of expected response")
        return EXIT_FAILURE;
    }
    if(compareBufToHexString(output_buffer, token, count)) {
        QPRINT(//"\nInstrument response FAILED to pass integrity check!\n"
                        "Expected Response:\n"
                        "%s\n\n"
                        , token);
        APPEND_ERR("Instrument response does not match expected response")
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int automaticConsole()
{
    if(setupLog()) {
        printf("Unable to set up log. Please strip all "
            "fprintf and recompile\n");
        perror("Error was");
        return EXIT_FAILURE;
    }

    // Overall test bookkeeping
    total_tests = 0;
    passed_tests = 0;

    // open CSV
    if(checkFileExists(command_sched_filename, SYSWIDE_PATH_LEN_MAX)) {
        printf("The schedule file %s does not exist.\n", command_sched_filename);
        printf("You can try doing something like \"-f `pwd`/filename\" for files"
            " in your current working directory.\n");
        return EXIT_FAILURE;
    }
    FILE * sched = fopen(command_sched_filename, "r");

    // getline variables
    size_t len = 0;
    char * line = NULL;
    ssize_t nbytes;

    // Keeping track of time
    struct timeval tv;
    uint64_t start_time, end_time;

    // tokenizing bookkeeping
    const char delimiters[] = ",";
    char *token;

    // get line
    while((nbytes = getline(&line, &len, sched)) > 0) {
        // To see how much time to wait at the end of the cycle, first get a start time.
        gettimeofday(&tv, NULL);
        start_time = tv.tv_sec * 1000000+tv.tv_usec;

        // Clear out bookkeeping
        memset((uint8_t*)&current_test, '\0', sizeof current_test);

        // delete trailing newline
        line[strcspn(line, "\n")] = '\0';

        // get all the hex chars until comma or EOF
        // reference: https://www.gnu.org/software/libc/manual/html_node/Finding-Tokens-in-a-String.html
        tokenizeScheduleLine(line, delimiters);

        token = current_test.command;

        size_t token_len = strlen(token);
        if(token_len % 2 || token_len == 0) {
            fprintf(stderr, "Invalid hex command -> must be of "
                        "even, nonzero length! command: %s\n"
                        , token);
            APPEND_ERR("Hex command must be even"
                        "/nonzero length")
            goto cleanup_iteration;
        }

        // place into hex buffer
        removeChar(token, ' '); // TODO: remove all non-hex!

        if(fillHexBufFromString(command_buffer, token, BIG_BUF_LEN)) {
            APPEND_ERR("Could not scan hex buffer from command string")
            goto cleanup_iteration;
        }

        // write to serial line/read response
        ssize_t write_len = strlen(token)/2;

//        // Write the command we write, and a comma, to the log
//        logDataAsHex(schedule_log, command_buffer, write_len);
//        fprintf(schedule_log, ",");

        if(writeCommandAndReadResponse(write_len)) {
            puts("Unable to write and read response");
            APPEND_ERR("Unable to write and read response");
            goto cleanup_iteration;
        }// else {
        //    logDataAsHex(schedule_log, output_buffer, count);
        //}

        //fprintf(schedule_log, "\n");
        //fflush(schedule_log);

        if(check_response) {
            current_test.status = (checkExpectedResponse());
        }


cleanup_iteration:

        total_tests++;
        if(!current_test.status) {
            passed_tests++;
        }
        informUser();
        logTest();

        gettimeofday(&tv, NULL);
        end_time = tv.tv_sec * 1000000 + tv.tv_usec;

        // usleep the number of microseconds we need to wait (or less)
        if((uint64_t)end_time > (uint64_t)start_time
            && ((uint64_t)end_time - (uint64_t)start_time 
                < (uint64_t)wait_time)) {
            usleep(wait_time - (end_time - start_time));
        }

    }
    if(passed_tests != total_tests) {
        printf("========================================\n");
        printf("Results: %zd/%zd tests passed\n", passed_tests, total_tests);
    } else {
        QPRINT("========================================\n");
        QPRINT("All tests passed!\n");
    }        
    
    return EXIT_SUCCESS;
}

void informUser()
{
    // Pass:
    // Command name
    // Command comment
    // Status: PASSED
    if(!current_test.status) {
        QPRINT("Command name: %s\n\n", current_test.name)
        QPRINT("Comment: %s\n\n", current_test.comment)
        QPRINT("Status: PASSED\n")
        QPRINT("----------------------------------------\n")
    } else {
    // Fail:
    // Command name
    // Command comment
    // Command hex
    // Expected response
    // Actual response
    // Status: FAILED
        printf("Command name: %s\n\n", current_test.name);
        printf("Comment: %s\n\n", current_test.comment);
        printf("Command hex:\n");
        print_struct(command_buffer, strlen(current_test.command)/2
            , COLS_PER_LINE);
        printf("\n");
        printf("Expected response:\n%s\n\n", current_test.expected_resp);
        printf("Actual response:\n");
        printf("\n");
        print_struct(output_buffer, count, COLS_PER_LINE);
        printf("Status: FAILED\n");
        printf("----------------------------------------\n");
    }
}

void logTest()
{
    // Log name
    APPEND_STRING_LOG(current_test.name)

    // Log command
    APPEND_STRING_LOG(current_test.command)

    // Log expected response
    // TODO: add NA when check_response is 0
    if(check_response) {
        APPEND_STRING_LOG(current_test.expected_resp)
    } else {
        APPEND_STRING_LOG("N/A")
    }

    // Log response
    APPEND_BUF_LOG(output_buffer, count)

    // Log status: pass/fail
    APPEND_STRING_LOG((current_test.status ? "FAILED" : "PASSED"))

    // Log error string
    APPEND_STRING_LOG(current_test.err_string)

    // Log comment
    APPEND_STRING_LOG(current_test.comment)

    fprintf(schedule_log, "\n");
    fflush(schedule_log);
}

void interactiveConsole()
{
    char * pt;
    int input_len = 0, write_len = 0;

    while(1){
        printf("Enter a hex command "
                "(do not preface with 0x, spaces allowed):\n");

        fgets((char*)input_buffer, BIG_BUF_LEN, stdin);

        // Remove trailing newline
        input_buffer[strcspn((char*)input_buffer, "\n")] = '\0';

        pt = (char*)input_buffer;
        removeChar(pt, ' ');

        input_len = strnlen(pt, BIG_BUF_LEN);
        input_len = input_len < 0 ? 0 : input_len;

        int hex_token_len = 2;

        write_len = read_ascii ? input_len : \
                    (input_len / hex_token_len);

        printf("We're going to write %d bytes \n", write_len);

        if(scanInputIntoCommand(pt)) {
            printf("Scan input failed\n");
            continue;
        }

        writeCommandAndReadResponse(write_len);
    }
}

int writeCommandAndReadResponse(int write_len)
{
    int ret = EXIT_SUCCESS;

    // Async IO setup
    fd_set rset;
    FD_ZERO(&rset);
    FD_SET(sfd, &rset);
    int maxfd = sfd + 1;
    int num_ready = 0;
    struct timeval tv;

    tv.tv_usec = serial_timeout * 1000;
    tv.tv_sec = 0;

    tcflush(sfd, TCIOFLUSH);
    if(write(sfd, command_buffer, write_len) != (ssize_t)write_len) {
        fprintf(stderr, "Write failed. Command buffer:\n");
        print_struct(command_buffer, write_len, 5);
        return EXIT_FAILURE;
    } else {
        QPRINT("Command written:\n");
        QPRINT_STRUCT(command_buffer, write_len, 5);
        QPRINT("\n");
    }
    sleep(1);

    size_t resp_len = BIG_BUF_LEN;
    if((ret = read(sfd, output_buffer, resp_len)) <= 0) {
        fprintf(stderr, "Read failed. buffer:\n");
        print_struct(output_buffer, MAX_BAD_RESP_LEN, 16);
        perror("Error message:");
        ret = EXIT_FAILURE;
        goto cleanup_log;
    } else {
        QPRINT("Response (%zu bytes):\n", ret);
        QPRINT_STRUCT(output_buffer, ret, COLS_PER_LINE);
        ret = 0;
        goto cleanup_log;
    }

cleanup_log:
    return ret;
}

void printBaudRate()
{
#define X(a,b)                                  \
    if(baud_rate == b) {                        \
        printf("Baud rate is: %s\n", a);        \
    }
    BAUD_RATES
#undef X
}

int setCustomBaudRate(const char * baud_str)
{
    // Leave room at the end for NUL char
    char * final_baud = calloc(strlen(baud_str)+1,1);
    strcat(final_baud, baud_str);

    baud_rate = atoi(final_baud);
    free(final_baud);

    return EXIT_SUCCESS;
}

int setBaudRate(const char * baud_str)
{
    // Leave room at the end for NUL and extra B char
    char * final_baud = calloc(strlen(baud_str)+2,1);
    final_baud[0] = 'B';
    if(*baud_str == 'B' || *baud_str == 'b') {
        baud_str++;
    }
    strcat(final_baud, baud_str);

    int keep_default = 1;
    // Here comes the most efficient hashtable ever
#define X(a,b)                                  \
    if(!strcmp(a,final_baud)) {                 \
        printf("Setting baud rate to %s\n", a); \
        baud_rate = b;                          \
        keep_default = 0;                       \
    }
    BAUD_RATES
#undef X

    if(keep_default) {
        printf("Unrecognized baud rate %s\n", baud_str);
    }
    printBaudRate();
    free(final_baud);

    return EXIT_SUCCESS;
}

int parseOptions(int argc, char ** argv)
{
    /**
     * ./serialConsole [device]
     *      [-f schedule_file]
     *      [-b baud_rate]
     *      [-k custom_baud_rate]
     *      [-m vmin]
     *      [-T vtime]
     *      [-r (RTC/CTS)]
     *      [-c (check response)]
     *      [-h (help)]
     *      [-w wait_time (in microseconds)]
     *      [-4 (RS485)]
     *      [-q (only print errors)]
     *      [-t serial_timeout]
     */
    int c;
    while((c = getopt(argc, argv, "f:b:k:m:t:rhw:4qT:c")) != -1) {
        switch(c) {
        case 'f':
            strcpy(command_sched_filename, optarg);
            console_mode = AUTOMATED_CONSOLE;
            break;
        case 'b':
            setBaudRate(optarg);
            break;
        case 'k':
            setCustomBaudRate(optarg);
            break;
        case 'm':
            if(parseUnsignedShort(optarg,(uint16_t*)&vmin)) {
                printf("Invalid vmin entered, using default: (%s->%s)\n"
                        , optarg, TOSTRING(SERIAL_VMIN));
                vmin = SERIAL_VMIN;
            }
            printf("vmin is now %d\n", vmin);
            break;
        case 'c':
            printf("Response Checking enabled\n");
            check_response = 1;
            break;
        case 'T':
            if(parseUnsignedShort(optarg,(uint16_t*)&vtime)) {
                printf("Invalid vtime entered, using default: (%s->%s)\n"
                        , optarg, TOSTRING(SERIAL_VTIME));
                vtime = SERIAL_VTIME;
            }
            printf("vtime is now %d\n", vtime);
            break;
        case 't':
            if(parseUnsignedShort(optarg, (uint16_t*)&serial_timeout)) {
                printf("Invalid serial_timeout entered, using default: (%s->%s)"
                        "\n"
                        , optarg, TOSTRING(SERIAL_SELECT_TIMEOUT));
                serial_timeout = SERIAL_SELECT_TIMEOUT;
            }
            // Ensuring sane bounds
            serial_timeout = serial_timeout > 999 ? 999 : serial_timeout;
            serial_timeout = serial_timeout < 5 ? 5 : serial_timeout;

            printf("serial_timeout is now %d\n", serial_timeout);
            break;
        case 'r':
            printf("Enabling RTS/CTS\n");
            enable_rtscts = 1;
            break;
        case 'h':
            usage();
            break;
        case 'w':
            if(parseUnsignedLong(optarg, (uint32_t*)&wait_time)) {
                printf("Invalid wait_time entered, using default: (%s->%s)\n"
                        , optarg, TOSTRING(SERIAL_WAIT_TIME));
                wait_time = SERIAL_WAIT_TIME;
            }
            printf("wait_time is now %d\n", wait_time);
            break;
        case '4':
            enable_rs485 = 1;
            printf("RS485 enabled\n");
            break;
        case 'q':
            quiet_mode = 1;
            break;
        case '?':
            printf("Invalid options or missing args, "
                    "please read the usage text:\n");
            usage();
            break;
        default:
            usage();
            exit(EXIT_FAILURE);
            break;
        }
    }

    if(optind < argc) {
        strcpy(device_name, argv[optind]);
    } else {
        printf("Using default serial device: %s\n", DEFAULT_SERIAL_DEVICE);
                strcpy(device_name, DEFAULT_SERIAL_DEVICE);
    }
    printf("Using serial device: %s\n", device_name);
    puts("========================================");
    return EXIT_SUCCESS;
}

int setupLog()
{
    char start_time [TIME_LEN + 1];
    timestamp(start_time, TIME_LEN + 1);
    char logname[SYSWIDE_PATH_LEN_MAX];
    strcpy(logname, basename(command_sched_filename));

    strcat(logname, ".log");
//    strcat(logname, start_time);

    schedule_log = fopen(logname, "w");
    if(schedule_log == NULL) {
        perror("Log failed to open");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

void enableRTSCTS(int fd)
{
    struct termios options;
    if(tcgetattr(fd, &options) != 0) {
        perror("Failed tcgetattr");
        return;
    }
    options.c_cflag |= CRTSCTS;
    if(tcsetattr(fd, TCSANOW, &options) != 0) {
        perror("Failed tcsetattr");
        return;
    }
}

int main(int argc, char ** argv)
{

    strcpy(program_name, basename(argv[0]));

    parseOptions(argc, argv);

    start_serial(&sfd, device_name, baud_rate, vmin, vtime);
    if(enable_rtscts) enableRTSCTS(sfd);
    if(enable_rs485) rs485_setup(sfd);

    switch(console_mode) {
        case INTERACTIVE_CONSOLE:
            interactiveConsole();
            break;
        case AUTOMATED_CONSOLE:
            automaticConsole();
            break;
        default:
            printf("Unrecognized console mode. Exiting (usage printed)\n");
            usage();
            return EXIT_FAILURE;
            break;
    }
    return EXIT_SUCCESS;
}
