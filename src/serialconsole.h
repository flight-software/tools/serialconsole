#ifndef SERIALCONSOLE_H_
#define SERIALCONSOLE_H_

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define TIME_LEN 14
#define SYSWIDE_PATH_LEN_MAX 215
//#include "uthash.h"
                                                                       
/**
 * Useful macros
 */
#define ARRAY_SIZE(arr) \
    (sizeof(arr)/sizeof(*arr))

#define HEX_FMT "%2hhx"
#define QPRINT(...)             \
    if(!quiet_mode) {           \
        printf(__VA_ARGS__);    \
    }
#define QPRINT_STRUCT(...)          \
    if(!quiet_mode) {               \
        print_struct(__VA_ARGS__);  \
    }
#define APPEND_STRING_LOG(msg)              \
    fprintf(schedule_log, "%s,", msg);      \
    fflush(schedule_log);
#define APPEND_BUF_LOG(buf, len)                        \
    logDataAsHex(schedule_log, buf, len);               \
    fprintf(schedule_log, ","); fflush(schedule_log);   
#define APPEND_ERR(msg)             \
    if(current_test.status) {       \
        strcat(current_test.err_string, "_");      \
    } else {                        \
        current_test.status = 1;    \
    }                               \
    strcat(current_test.err_string, msg);

/** 
 * Console Settings                                                    
 */     

#define DEFAULT_SERIAL_DEVICE "/dev/ttyUSB0" /* if no serial device passed in,
                                                open this one */
#define SERIAL_VMIN 1   /* # bytes to consider transmission complete */
#define SERIAL_VTIME 5  /* tenths of second silent to consider 
                          transmission complete */
#define SERIAL_BAUD() B115200 /* baud rate to use, see man 3 termios */
#define SERIAL_BAUD_STR "115200"
#define SERIAL_WAIT_TIME 50000 /* microseconds to wait b/t
                                  automated serial tx
                                  */
#define SERIAL_SELECT_TIMEOUT 10

/**
 * User Interface defaults
 */
#define COLS_PER_LINE 10    /**
                             * when printing buffers, use this many columns
                             * per line
                             */
#define MAX_BAD_RESP_LEN 256 /** 
                              * print at most this many bytes 
                              * from input/output buffers when
                              * we fail to read or write
                              */
#define INTERACTIVE_CONSOLE 0
#define AUTOMATED_CONSOLE 1


/**
 * Configuration variables (set to defaults in .c where applicable)
 */

extern char device_name[256];
extern char program_name[SYSWIDE_PATH_LEN_MAX];
extern char command_sched_filename[SYSWIDE_PATH_LEN_MAX];
extern int vmin;
extern int vtime;
extern speed_t baud_rate;
extern int enable_rtscts;
extern int enable_rs485;
extern int wait_time;
extern int read_ascii;
extern int console_mode;
extern int quiet_mode;
extern int serial_timeout;

/**
 * Function declarations
 */
void usage();
int scanInputIntoCommand(char*);
void interactiveConsole();
int writeCommandAndReadResponse(int);
int parseOptions(int, char**);
void printBaudRate();
int setCustomBaudRate(const char *);
int setBaudRate(const char *);
int automaticConsole();
int setupLog();
void logTest();
int tokenizeScheduleLine(char * line, const char delimiters[]);
void informUser();
int checkExpectedResponse();

#define TOK(x) X(#x, x)
#define BAUD_RATES \
        TOK(B50)	\
        TOK(B75)	\
        TOK(B110)	\
        TOK(B134)	\
        TOK(B150)	\
        TOK(B200)	\
        TOK(B300)	\
        TOK(B600)	\
        TOK(B1200)	\
        TOK(B1800)	\
        TOK(B2400)	\
        TOK(B4800)	\
        TOK(B9600)	\
        TOK(B19200)	\
        TOK(B38400)	\
        TOK(B57600)	\
        TOK(B115200)	\
        TOK(B230400)

#endif
